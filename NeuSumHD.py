import random
import torch
import logging
import re
import math
import itertools
import glob
import pickle
import time
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn

from rouge import rouge_score
from scipy.special import softmax
from operator import itemgetter
from torch.nn.utils.rnn import *
from torch.autograd import Variable


class Story:
    def __init__(self, file):
        self.link = ''
        self.title = ''
        self.description = ''
        self.highlights = []
        self.body = []
        self.read(file)

    def __str__(self):
        return (
            self.link + '\n' +
            self.title + '\n' +
            self.description + '\n' +
            '\n'.join(['@highlight\n' + highlight for highlight in self.highlights]) + '\n' +
            '\n'.join(self.body)
        )

    def read(self, file):
        with open(file, 'r', encoding='utf-8') as fin:
            lines = fin.readlines()
            self.link = lines[0].strip()
            self.title = lines[1].strip()
            self.description = lines[2].strip()

            is_highlight = False
            for line in lines[3:]:
                line = line.strip()
                if is_highlight:
                    if len(line):
                        self.highlights.append(line)
                    is_highlight = False
                elif '@highlight' in line:
                    is_highlight = True
                else:
                    if len(line):
                        self.body.append(line)


def read_document(file_path: str):
    """Read a document in lines.
    Args:
        file_path: path of file on disk
    Returns:
        lines: list(sentence)
    """

    story = Story(file_path)
    return story


def process_story(story):
    """Seperate and extract article and summary from a story.
    Args:
        lines: list(sentence), represents a story

    Returns:
        article: list(sentence), represents article content of a story
        summary: list(sentence)
    """

    article = story.body
    summary = story.highlights
    if len(summary) < 3:
        description_split = story.description.split(' . ')
        for i in range(min(len(description_split), 3-len(summary))):
            summary.append(description_split[i] + ' .')

    return article, summary


def split_paragraphs(article: list, max_sent_len=100):
    processed_article = []
    sent_lengths = []
    # end_tokens = ['.', '!', '?']
    end_tokens = [33, 34, 39, 46, 63, 8230]  # colon?

    for paragraph in article:
        lines = []

        line = []
        within_quote = 0
        impure_paragraph = False
        for token in paragraph.split(' '):
            line += [token]
            if token.find("``") > -1:
                within_quote += 1
            elif token.find("''") > -1:
                if within_quote > 0:
                    within_quote -= 1
                else:
                    impure_paragraph = True
            elif ord(token[0]) in end_tokens and not within_quote:
                if len(line) <= max_sent_len:
                    if len(line) < 4 and len(lines):
                        sent_lengths[-1] += len(line)
                        lines[-1] += ' ' + ' '.join(line)
                    elif len(line) > 2:
                        sent_lengths += [len(line)]
                        lines += [' '.join(line)]
                line = []
        if within_quote is 1:
            line += ["''"]
        elif within_quote > 1:
            impure_paragraph = True

        if len(line) > 3:
            lines += [' '.join(line)]

        if not impure_paragraph:
            processed_article += lines

    return processed_article, sent_lengths


def load_documents(stories=[], total_doc=1, offset=0, plot=True, max_doc_len=80, max_sent_len=100, _do_split_paragraphs=True):

    docs = []
    files = []
    global doc_lengths
    doc_lengths = []    # (map) doc_len: int -> count: int
    global sent_lengths
    sent_lengths = []   # (map) sent_len: int -> count: int

    idx = 0
    while len(docs) < total_doc and offset + idx < len(stories):

        file_path = stories[idx + offset]
        story = read_document(file_path)
        idx += 1

        article, summary = process_story(story)

        if _do_split_paragraphs:
            article, sent_lengths_of_doc = split_paragraphs(
                article, max_sent_len)
        else:
            article = [line for line in article if len(
                line.split(' ')) <= max_sent_len]
            sent_lengths_of_doc = [len(line.split(' ')) for line in article]

        doc_len = len(article)

        # few stories are found to have empty articles or articles with lots of sentences
        if not doc_len or doc_len > max_doc_len or not len(summary):
            continue

        docs.append([article, summary])
        files.append(file_path)
        doc_lengths += [doc_len]
        sent_lengths += sent_lengths_of_doc

    if plot:
        plt.hist(doc_lengths, bins=max(*doc_lengths) - 2, density=1)
        plt.ylim(top=0.07)
        plt.ylabel('Fraction of Data')
        plt.xlabel('Document Length (Number of Sentences)')
        plt.savefig(docs_histogram_save_path +
                    ("_%d.png" % total_doc), dpi=500)
        plt.show()
        plt.clf()
        plt.hist(sent_lengths, bins=max(*sent_lengths) - 2, density=1)
        plt.ylim(top=0.06)
        plt.ylabel('Fraction of Data')
        plt.xlabel('Sentence Length (Number of Tokens)')
        plt.savefig(sent_histogram_save_path +
                    ("_%d.png" % total_doc), dpi=500)
        plt.show()

    # shape(limit_doc, 2->[article: list, summary: list])
    return docs, files


stories_input_path = './aajtak_stories_tokenised'
docs_save_path = './db/preprocessed_docs.p'
docs_histogram_save_path = './db/figures/doc_len'
sent_histogram_save_path = './db/figures/sent_len'

stories = sorted(glob.glob(stories_input_path + '/*'))

doc_lengths = []
sent_lengths = []

start = time.time()
processed_docs, files = load_documents(stories, total_doc=80000)
print("Processed data in: %fs" % (time.time() - start))

pickle.dump(docs, open(docs_save_path, 'wb'))


def nCr(n, r):
    fact = math.factorial
    return (fact(n) // (fact(r) * fact(n-r)))


suffixes = [
    ["ो", "े", "ू", "ु", "ी", "ि", "ा"],
    ["कर", "ाओ", "िए", "ाई", "ाए", "ने", "नी", "ना",
     "ते", "ीं", "ती", "ता", "ाँ", "ां", "ों", "ें"],
    ["ाकर", "ाइए", "ाईं", "ाया", "ेगी", "ेगा", "ोगी", "ोगे", "ाने", "ाना",
     "ाते", "ाती", "ाता", "तीं", "ाओं", "ाएं", "ुओं", "ुएं", "ुआं"],
    ["ाएगी", "ाएगा", "ाओगी", "ाओगे", "एंगी", "ेंगी", "एंगे", "ेंगे", "ूंगी",
     "ूंगा", "ातीं", "नाओं", "नाएं", "ताओं", "ताएं", "ियाँ", "ियों", "ियां"],
    ["ाएंगी", "ाएंगे", "ाऊंगी", "ाऊंगा", "ाइयाँ", "ाइयों", "ाइयां"],
]


def hi_stem(word):
    for L in range(1, 6):
        if len(word) > L + 1:
            for suf in suffixes[L - 1]:
                if word.endswith(suf):
                    return word[:-L]
    return word


def _pre_rouge_sanitize(sentences):
    tokenised_sents = []
    for sentence in sentences:
        tokens = []
        sent_tokens = sentence.split(' ')
        for token in sent_tokens:
            tokens += [hi_stem(token)]
        tokenised_sents += [tokens]
    return tokenised_sents


def _get_rouge(summary, prediction):
    scores = rouge_score(summary, prediction, ["rouge2"])
    rouge2f1 = scores['rouge2']['f1']

    return rouge2f1


def _get_rouge_gains(docs: list, max_order_length=3):
    """Gives the sequence of extracted sentences
    optimised to give the best rouge overlap with gold summary
    Args:
        docs: list, shape (total_docs, 2 -> [article:list , summary: list])
    """
    all_rouge_gains = []
    all_oracle_order = []
    all_rouge_scores = []
    stemmed_docs = []

    for doc_idx, doc in enumerate(docs):
        if doc_idx % 1000 == 0 and doc_idx:
            elapsed = time.time() - start
            time_per_doc = elapsed / doc_idx
            eta = (len(docs) - doc_idx) * time_per_doc
            print("%d docs labeled in %fs" % (doc_idx, elapsed))
            print("ETA: %dmin %fs" % (eta // 60, eta % 60))

        # sentences, summary = doc
        sentences = _pre_rouge_sanitize(doc[0])
        summary = _pre_rouge_sanitize(doc[1])

        stemmed_docs.append([[' '.join(sentence) for sentence in sentences], [
                            ' '.join(sentence) for sentence in summary]])

        _order = []
        _order_total_rouge = 0.0
        _rouge_gains = []
        _total_sentences = len(sentences)

        while len(_order) < max_order_length:
            _candidates = [(_order + [idx]) for idx in range(_total_sentences)]

            _rouge_gain = [(_get_rouge(summary, [sentences[idx] for idx in _candidate]) -
                            _order_total_rouge) for _candidate in _candidates]

            _max_gain = max(_rouge_gain)
            _min_gain = min(_rouge_gain)
            _max_index = _rouge_gain.index(_max_gain)

            if _max_gain > 0.0 and _max_index not in _order:
                _order += [_max_index]
                _order_total_rouge += _max_gain

                _rouge_gain = (np.array(_rouge_gain, dtype=float) -
                               _min_gain) / (_max_gain - _min_gain)
                _rouge_gain = softmax(_rouge_gain * 5)
                # _rouge_gains.append(_rouge_gain.tolist() + [0.0])
                _rouge_gains.append(_rouge_gain.tolist())

            else:
                # _rouge_gains.append([0.0] * len(sentences) + [1.0])
                break

        # _lead2 = _get_rouge(summary, sentences[:3])
        # _lead3 = _get_rouge(summary, sentences[:4])

        all_rouge_scores.append(_order_total_rouge)
        all_oracle_order.append(_order)
        all_rouge_gains.append(_rouge_gains)

    return all_rouge_gains, all_oracle_order, all_rouge_scores, stemmed_docs


docs_input_path = './db/preprocessed_docs.p'
label_save_path = './db/labeled_data.p'
rouge_histogram_save_path = './db/figures/rouge'
rouge_plot = True

processed_docs = pickle.load(open(docs_input_path, 'rb'))

start = time.time()
gains, orders, rouges, docs = _get_rouge_gains(processed_docs)
print("Labeled data in: %fs" % (time.time() - start))

pickle.dump((gains, orders, rouges), open(label_save_path, 'wb'))

if rouge_plot:
    plt.hist(np.array(rouges) * 100, bins=100, density=1)
    plt.ylabel('Fraction of Data')
    plt.xlabel('Rouge Bigram F1 Score')
    plt.savefig(rouge_histogram_save_path + ('_%d.png' %
                                             len(processed_docs)), dpi=500)
    plt.show()
    plt.clf()

positions = []
for order in orders:
    positions += order

plt.hist(np.array(positions) + 1, bins=100, density=1)
plt.ylabel('Fraction of Data')
plt.xlabel('Sentence Position(index)')
plt.savefig(('./db/sentence_position_%d.png' % len(processed_docs)), dpi=500)
plt.show()
plt.clf()

stemmed_docs_save_path = './db/stemmed_docs.p'
pickle.dump(docs, open(stemmed_docs_save_path, 'wb'))


class Vocab:
    def __init__(self, corpus=None):
        self.special = {
            '<pad>': 0,
            '<unk>': 1,
            '<eod>': 2
        }
        self._reset()
        if corpus is not None:
            self.load_from_corpus(corpus)

    def _reset(self):
        self.word2idx = self.special.copy()
        self.idx2word = {idx: word for word, idx in self.word2idx.items()}
        self.frequency = {word: 1 for word in self.word2idx}

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.idx2word.get(key, '<unk>')
        elif isinstance(key, str):
            return self.word2idx.get(key, 1)
        else:
            raise ValueError('Invalid key.')

    def __len__(self):
        return len(self.word2idx)

    def add(self, word: str, initial_frequency=1):
        if word not in self.word2idx:
            next_index = len(self)

            self.word2idx[word] = next_index
            self.idx2word[next_index] = word

            self.frequency[word] = initial_frequency

        else:
            self.frequency[word] += 1

        return self[word]

    def slash(self, limit: int):
        sorted_frequency = sorted(
            self.frequency.items(), key=lambda _freq: _freq[1], reverse=True)

        self._reset()

        for idx, (word, freq) in enumerate(sorted_frequency):
            if idx >= (limit - len(self.special)):
                break
            self.add(word, freq)

        print("Slashed: %d words" % (len(sorted_frequency) - len(self)))
        print("Coverage: %.2f %%" % (100 * len(self) / len(sorted_frequency)))

        return self

    def load_from_corpus(self, docs: list):
        for doc in docs:
            article, summary = doc
            for sentence in article + summary:
                for word in sentence.split(' '):
                    self.add(word)

        return self

    def to_vector(self, sentences: list):
        output = []
        for sentence in sentences:
            sentence2vec = []
            for word in sentence.split(' '):
                sentence2vec += [self[word]]
            output += [sentence2vec]

        return output

    def to_sentences(self, doc_vec: list):
        output = []
        for sent_vec in doc_vec:
            vec2sentence = []
            for idx in sent_vec:
                vec2sentence += [self[idx]]
            output += [' '.join(vec2sentence)]

        return output


docs_input_path = './db/preprocessed_docs.p'
stemmed_docs_input_path = './db/stemmed_docs.p'

vocab_save_path = './db/vocab.p'
vocab_limit = 1e5

# shape (total_docs, 2)
plain_docs = pickle.load(open(docs_input_path, 'rb'))
docs = pickle.load(open(stemmed_docs_input_path, 'rb'))

start = time.time()
vocab = Vocab(docs).slash(vocab_limit)
print("Vocab prepared in: %fs" % (time.time() - start))

pickle.dump(vocab, open(vocab_save_path, 'wb'))


docs_input_path = './db/preprocessed_docs.p'
stemmed_docs_input_path = './db/stemmed_docs.p'

# shape (total_docs, 2)
plain_docs = pickle.load(open(docs_input_path, 'rb'))
docs = pickle.load(open(stemmed_docs_input_path, 'rb'))

# global_gains, orders -> shape: (total_docs, selected_sentences)
global_gains, orders, _ = pickle.load(open('./db/labeled_data.p', 'rb'))

# type -> Vocab
vocab = pickle.load(open('./db/vocab.p', 'rb'))

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

docs_vectorised = []  # shape(number_of_docs, doc_length)

tic = time.time()
for article, _ in docs:
    # article.append('<eod>')  # a sentence "<eod>"
    doc = vocab.to_vector(article)
    docs_vectorised.append(doc)
toc = time.time()
print('Documents vectorised in: %fs' % (toc-tic))
print()

# g = torch.tensor(gains, requires_grad=False)

# min_g = g.min(dim=2, keepdim=True).values
# max_g = g.max(dim=2, keepdim=True).values
# g̃ = (g - min_g) / (max_g - min_g)

# Q = torch.softmax(g̃ * 5, dim=2)
# NOTE: the above operations are shifted in labelling process, in `label_training_data.py`

labels = [torch.tensor(doc_gains, requires_grad=False, device=device)
          for doc_gains in global_gains]

for doc_gains, doc in zip(global_gains, docs_vectorised):
    assert not len(doc_gains) or len(doc_gains[0]) == len(doc)

assert len(labels) == len(docs_vectorised) and len(orders) == len(labels)


class NeuSum(nn.Module):
    def __init__(self, device, test_mode=False):
        super(NeuSum, self).__init__()

        self.word_embedding = nn.Embedding(len(vocab), 50, vocab['<pad>'])

        self.sent_encoder = nn.GRU(50, 128, bidirectional=True)
        self.sent_dropout = nn.Dropout(0.3)

        self.doc_encoder = nn.GRU(256, 128, bidirectional=True)
        self.doc_dropout = nn.Dropout(0.2)

        self.scorer_gru = nn.GRUCell(256, 256)
        self.linear_m = nn.Linear(128, 256)
        self.linear_q = nn.Linear(256, 256)
        self.linear_d = nn.Linear(256, 256)
        self.linear_s = nn.Linear(256, 1)

        self.device = device
        self.test_mode = test_mode

    def load_embeddings_from_pretrained(self, file_path, vocab):
        weight = torch.zeros(
            len(vocab), 50, dtype=torch.float, device=self.device)
        nn.init.xavier_normal_(weight, math.sqrt(3))

        with torch.no_grad():
            weight[vocab['<pad>']].fill_(0)

            words_matched = 0
            pretrained_file = open(file_path, 'r')
            for line in pretrained_file:
                line = line.strip().split(' ')

                word = line[0]
                id = vocab[word]

                if id != vocab['<unk>'] and len(line[1:]) is 50:
                    vector = torch.tensor(
                        np.array(line[1:], dtype=float), device=device)
                    words_matched += 1
                    weight[id].copy_(vector)

            print("Pretained embeddings coverage: %.2f%%" %
                  (100 * words_matched / len(vocab)))

        self.word_embedding = torch.nn.Embedding.from_pretrained(
            weight, freeze=True, padding_idx=vocab['<pad>'])

    def pad_data(self, docs_vectorised):
        batch = []  # shape(batch_size * max_doc_len, max_sent_len)

        # document level padding
        doc_lengths = [len(doc) for doc in docs_vectorised]
        max_doc_len = max(doc_lengths)

        for doc in docs_vectorised:
            batch += doc + [[0]] * (max_doc_len - len(doc))

        # sentence level padding
        # shape(batch_size * max_doc_len)
        sentence_lengths = [len(sentence) for sentence in batch]
        max_sentence_len = max(sentence_lengths)

        batch = torch.tensor([
            sentence + [0] * (max_sentence_len - len(sentence))
            for sentence in batch
        ], dtype=torch.long, device=self.device)

        return batch, doc_lengths, sentence_lengths

    def sent_encoding(self, batch, sentence_lengths):
        # shape (batch_size * max_doc_len, max_sent_len, dim=50)
        embeddings = self.word_embedding(batch)

        sent_enc_inp = pack_padded_sequence(
            embeddings,
            sentence_lengths,
            batch_first=True,
            enforce_sorted=False
        )
        _, sent_enc_out = self.sent_encoder(sent_enc_inp)
        # shape (2, batch_size*max_doc_len, dim=128), already unpacked
        sent_enc_out = self.sent_dropout(sent_enc_out)

        return sent_enc_out

    def doc_encoding(self, sent_enc_out, doc_lengths):
        batch_size = len(doc_lengths)
        max_doc_len = max(doc_lengths)

        doc_enc_inp = pack_padded_sequence(
            sent_enc_out  # shape (2, batch_size*max_doc_len, dim=128)
            .transpose(1, 0)  # shape (batch_size*max_doc_len, 2, dim=128)
            # shape (batch_size, max_doc_len, dim=256)
            .reshape(batch_size, max_doc_len, -1),
            # .contiguous()  # allocate contiguous memory for efficiency,
            doc_lengths,
            batch_first=True,
            enforce_sorted=False
        )

        doc_enc_out, _ = self.doc_encoder(doc_enc_inp)
        # after unpacking -> shape (batch_size, max_doc_len, dim=256)
        doc_enc_out, _ = pad_packed_sequence(doc_enc_out, batch_first=True)
        doc_enc_out = self.doc_dropout(doc_enc_out)

        return doc_enc_out

    def score_sentences(self, doc_enc_out, orders_target, doc_lengths):
        batch_size = len(doc_lengths)
        max_doc_len = max(doc_lengths)

        # a flat view of doc_encoding, shape(batch_size*max_doc_len, dim=256)
        sentence_representations = doc_enc_out.view(batch_size*max_doc_len, -1)

        # "<--s1" : shape (batch_size, dim=128)
        linear_m_inp = doc_enc_out[:, 0, 128:]
        # shape (batch_size, dim=256)
        linear_m_out = self.linear_m(linear_m_inp)

        scr_hdn_lyr = torch.tanh(linear_m_out)  # shape (batch_size, dim=256)
        # shape (batch_size, dim=256)
        prev_sel_sent = torch.zeros(batch_size, 256, device=self.device)

        linear_d_output = self.linear_d(sentence_representations)

        order_length = 3
        # shape (order_length, batch_size* max_doc_len)
        gains_evaluated = torch.tensor([], device=self.device)

        for idx in range(order_length):
            # shape (batch_size, dim=256)
            scr_hdn_lyr = self.scorer_gru(prev_sel_sent, scr_hdn_lyr)

            linear_q_output = (
                self.linear_q(scr_hdn_lyr)  # shape (batch_size, dim=256)
                # shape (batch_size, max_doc_len * dim=256)
                .repeat(1, max_doc_len)
                # shape (batch_size, max_doc_len, dim=256)
                .view(batch_size*max_doc_len, -1)
                .contiguous()  # allocate contiguous memory for efficiency
            )  # shape (batch_size*max_doc_len, dim=256)

            # shape(batch_size*max_doc_len, dim=256)
            linear_s_input = torch.tanh(linear_q_output + linear_d_output)

            # shape(batch_size*max_doc_len, dim=1)
            scores = self.linear_s(linear_s_input)

            # shape(batch_size * max_doc_len, idx+1)
            gains_evaluated = torch.cat((gains_evaluated, scores), dim=1)

            scores = scores.view(batch_size, max_doc_len)
            sent_indices_to_pick = torch.tensor(
                [
                    # index of should-be-selected sentences in representation
                    doc_idx * max_doc_len + order[idx]
                    if idx < len(order) else doc_idx * max_doc_len + doc_lengths[doc_idx] - 1
                    for doc_idx, order in enumerate(orders_target)
                ],
                device=self.device
            ) if not self.test_mode else torch.tensor(
                [
                    # index of should-be-selected sentences in representation
                    doc_idx * max_doc_len + \
                    doc_scores[:doc_lengths[doc_idx]].max(
                        dim=0).indices.item()
                    for doc_idx, doc_scores in enumerate(scores)
                ],
                device=self.device
            )

            prev_sel_sent = sentence_representations.index_select(
                0, sent_indices_to_pick)  # shape(batch_size*max_doc_len, dim=1)

        return (gains_evaluated  # shape(batch_size * max_doc_len, order_length)
                # shape(batch_size , max_doc_len, order_length)
                .view(batch_size, max_doc_len, order_length)
                .permute(0, 2, 1))  # shape(batch_size , order_length, max_doc_len)

    def forward(self, docs_vectorised, orders_target=None):
        batch_size = len(docs_vectorised)
        assert orders_target is not None or self.test_mode

        # shape(batch_size * max_doc_len, max_sent_len), shape(batch_size), shape(batch_size * max_doc_len)
        batch, doc_lengths, sentence_lengths = self.pad_data(docs_vectorised)

        # shape (2, batch_size*max_doc_len, dim=128)
        sent_enc_out = self.sent_encoding(batch, sentence_lengths)

        # shape (batch_size, max_doc_len, dim=256)
        doc_enc_out = self.doc_encoding(sent_enc_out, doc_lengths)

        # shape(batch_size , order_length, max_doc_len)
        gains_evaluated = self.score_sentences(
            doc_enc_out, orders_target, doc_lengths)

        return gains_evaluated

    def calculate_loss(self, gains_evaluated, labels):
        loss = torch.tensor(0.0, device=device)

        for doc_idx, doc_gains in enumerate(batch_labels):
            summary_sents = len(doc_gains)
            if not summary_sents:
                continue
            doc_len = len(doc_gains[0])

            # predicted_order_sents = gains_evaluated[doc_idx, :summary_sents, :doc_len].max(dim=1).indices.tolist()
            # target_order_sents = doc_gains.max(dim=1).indices.tolist()
            # print(predicted_order_sents)
            # print(target_order_sents)

            logP = torch.log_softmax(
                gains_evaluated[doc_idx, :summary_sents, :doc_len], dim=1)

            loss += nn.KLDivLoss(reduction='sum')(logP, doc_gains)

        return loss


# ---- Setup Logger ----
logger = logging.getLogger()
logger.setLevel(logging.INFO)

file_handler = logging.FileHandler(
    './db/logs/' + time.strftime("%Y%m%d-%H%M%S") + "_%d-docs.log.txt" % len(docs))
console_handler = logging.StreamHandler()

formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s')
console_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)

for handler in list(logger.handlers):
    logger.removeHandler(handler)
assert len(logger.handlers) is 0

logger.addHandler(console_handler)
logger.addHandler(file_handler)
# ---- ^ Setup Logger ----

logger.info("Started Training")


def index_select(array, indices):
    return [array[idx] for idx in indices]


neusum = NeuSum(device)
neusum = neusum.to(device)

optimizer = torch.optim.Adam(neusum.parameters())

batch_cut = 64
number_of_docs = len(docs)
number_of_epochs = 100
loss_history = []
validation_loss_history = []

load_model_from_epoch = 82
if load_model_from_epoch:
    neusum.load_state_dict(torch.load(
        './db/models/epoch_%d_%d_model.p' % (load_model_from_epoch, number_of_docs)))
    optimizer.load_state_dict(torch.load(
        './db/models/epoch_%d_%d_optim.p' % (load_model_from_epoch, number_of_docs)))
    loss_history, validation_loss_history = pickle.load(open(
        './db/models/loss_history_%d_%d.p' % (load_model_from_epoch, number_of_docs), 'rb'))
else:
    with torch.no_grad():
        for pmtr_name, parameter in neusum.named_parameters():
            if parameter.dim() == 1:
                nn.init.normal_(parameter, 0, math.sqrt(6 / parameter.size(0)))
            else:
                nn.init.xavier_normal_(parameter, math.sqrt(3))

training_start = time.time()
save_epoch_after = 2
valid_data_split = math.floor(number_of_docs * 0.9)

if not load_model_from_epoch:
    with torch.no_grad():
        validate_doc_indices = np.arange(valid_data_split, number_of_docs)
        number_of_batches = math.ceil(
            (number_of_docs - valid_data_split) / batch_cut)

        neusum.eval()
        epoch_valid_loss = 0.0
        for batch_idx in range(number_of_batches):
            batch_doc_indices = validate_doc_indices[batch_idx *
                                                     batch_cut: (batch_idx + 1) * batch_cut]

            _batch_docs_vectorised = index_select(
                docs_vectorised, batch_doc_indices)
            _batch_orders = index_select(orders, batch_doc_indices)
            # shape(batch_size , order_length, max_doc_len)
            gains_evaluated = neusum(_batch_docs_vectorised, _batch_orders)

            batch_labels = index_select(labels, batch_doc_indices)
            # shape(1)
            loss = neusum.calculate_loss(gains_evaluated, batch_labels)

            epoch_valid_loss += loss

        epoch_valid_loss /= number_of_docs - valid_data_split
        validation_loss_history.append(epoch_valid_loss.item())

        logger.info("Initial validation loss: %f" % epoch_valid_loss)

for epoch_idx in range(load_model_from_epoch + 1, number_of_epochs + 1):
    epoch_loss = 0.0
    epoch_valid_loss = 0.0
    # successful_prediction = 0

    document_indices = np.arange(valid_data_split)
    np.random.shuffle(document_indices)
    number_of_batches = math.ceil(valid_data_split / batch_cut)

    neusum.train()
    for batch_idx in range(number_of_batches):
        batch_doc_indices = document_indices[batch_idx *
                                             batch_cut: (batch_idx + 1) * batch_cut]

        optimizer.zero_grad()  # clear the grad. graph

        _batch_docs_vectorised = index_select(
            docs_vectorised, batch_doc_indices)
        _batch_orders = index_select(orders, batch_doc_indices)
        # shape(batch_size , order_length, max_doc_len)
        gains_evaluated = neusum(_batch_docs_vectorised, _batch_orders)

        batch_labels = index_select(labels, batch_doc_indices)
        # shape(1)
        loss = neusum.calculate_loss(gains_evaluated, batch_labels)

        loss.backward()

        nn.utils.clip_grad_norm_(neusum.parameters(), 5)

        optimizer.step()

        epoch_loss += loss

    with torch.no_grad():
        validate_doc_indices = np.arange(valid_data_split, number_of_docs)
        number_of_batches = math.ceil(
            (number_of_docs - valid_data_split) / batch_cut)

        neusum.eval()
        for batch_idx in range(number_of_batches):
            batch_doc_indices = validate_doc_indices[batch_idx *
                                                     batch_cut: (batch_idx + 1) * batch_cut]

            _batch_docs_vectorised = index_select(
                docs_vectorised, batch_doc_indices)
            _batch_orders = index_select(orders, batch_doc_indices)
            # shape(batch_size , order_length, max_doc_len)
            gains_evaluated = neusum(_batch_docs_vectorised, _batch_orders)

            batch_labels = index_select(labels, batch_doc_indices)
            # shape(1)
            loss = neusum.calculate_loss(gains_evaluated, batch_labels)

            epoch_valid_loss += loss

    epoch_loss /= valid_data_split - 0
    loss_history.append(epoch_loss.item())

    epoch_valid_loss /= number_of_docs - valid_data_split
    validation_loss_history.append(epoch_valid_loss.item())

    elapsed = time.time() - training_start
    time_per_epoch = elapsed / (epoch_idx - load_model_from_epoch)
    eta = (number_of_epochs - epoch_idx) * time_per_epoch
    dys = eta // 86400
    hrs = eta // 3600 % 24
    _min = eta // 60 % 60
    sec = eta % 60

    logger.info("Epoch: %d" % epoch_idx)
    # print("Successful Overlap: %.2f%%" % (100 * successful_prediction / number_of_docs))
    logger.info("Training Loss: %f" % epoch_loss)
    logger.info("Validation Loss: %f" % epoch_valid_loss)
    print("Expected training duration: %dd %dh %dmin %fs" %
          (dys, hrs, _min, sec))
    print()

    if (epoch_idx) % save_epoch_after is 0:
        torch.save(neusum.state_dict(
        ), ('./db/models/epoch_%d_%d_model.p' % (epoch_idx, number_of_docs)))
        torch.save(optimizer.state_dict(
        ), ('./db/models/epoch_%d_%d_optim.p' % (epoch_idx, number_of_docs)))
        print('---Model Saved---')

        pickle.dump((loss_history, validation_loss_history), open(
            './db/models/loss_history_%d_%d.p' % (epoch_idx, number_of_docs), 'wb'))
        plt.plot(loss_history, label="Training Loss")
        plt.plot(validation_loss_history, label="Validation Loss")
        plt.ylabel('Loss (per doc)')
        plt.xlabel('Epoch')
        if epoch_idx < 25:
            plt.xticks(range(epoch_idx + 2))
        plt.grid()
        plt.legend()
        plt.savefig('./db/figures/loss_history_%d.png' %
                    number_of_docs, dpi=500)
        plt.show()

    # break

loss_history, validation_loss_history = pickle.load(
    open('./db/models/loss_history_%d_%d.p' % (100, number_of_docs), 'rb'))
plt.plot(loss_history, label="Training Loss")
# plt.plot(validation_loss_history, label="Validation Loss")
plt.ylabel('Loss (per doc)')
plt.xlabel('Epoch')
# if epoch_idx < 25:
# plt.xticks(range(epoch_idx + 2))
plt.grid()
plt.legend()
plt.savefig('./db/figures/loss_history_fig_%d.png' % number_of_docs, dpi=500)
plt.show()


def index_select(array, indices):
    return [array[idx] for idx in indices]


def _pre_rouge_sanitize(sentences):
    tokenised_sents = []
    for sentence in sentences:
        tokens = []
        sent_tokens = sentence.split(' ')
        for token in sent_tokens:
            tokens += [hi_stem(token)]
        tokenised_sents += [tokens]
    return tokenised_sents


def tokenise(sentences):
    tokenised_sents = []
    for sentence in sentences:
        sent_tokens = sentence.split(' ')
        tokenised_sents.append(sent_tokens)
    return tokenised_sents


def _get_rouge_2(summary, prediction):
    scores = rouge_score(tokenise(summary), tokenise(prediction), ["rouge2"])
    rouge2f1 = scores['rouge2']['f1']

    return rouge2f1


for i in range(37, 38):
    neusum = NeuSum(device, test_mode=True)
    neusum = neusum.to(device)

    batch_cut = 512
    number_of_docs = len(docs)

    load_model_from_epoch = i
    load_model_from_file = './db/models/epoch_%d_%d_model.p' % (
        load_model_from_epoch, number_of_docs)
    neusum.load_state_dict(torch.load(load_model_from_file))

    test_data_split = math.floor(number_of_docs * 0.9)

    test_doc_indices = np.arange(test_data_split, number_of_docs)
    number_of_batches = math.ceil(
        (number_of_docs - test_data_split) / batch_cut)

    neusum.eval()

    successful_prediction = {}
    rouge_scores = []
    target_rouge_scores = []
    random_rouge_scores = []
    lead3_rouge_scores = []
    lead2_rouge_scores = []

    selected_positions = []

    for batch_idx in range(number_of_batches):
        batch_doc_indices = test_doc_indices[batch_idx *
                                             batch_cut: (batch_idx + 1) * batch_cut]

        _batch_docs_vectorised = index_select(
            docs_vectorised, batch_doc_indices)

        # shape(batch_size , order_length, max_doc_len)
        gains_evaluated = neusum(_batch_docs_vectorised)

        batch_labels = index_select(labels, batch_doc_indices)
        for doc_idx, doc_gains in enumerate(batch_labels):
            summary_sents = len(doc_gains)
            if not summary_sents:
                continue
            doc_len = len(doc_gains[0])

            predicted_order_sents = gains_evaluated[doc_idx, :summary_sents, :doc_len].max(
                dim=1).indices.tolist()
            target_order_sents = doc_gains.max(dim=1).indices.tolist()

            # remove duplicacy
            selected_sentences = []
            for sent_idx in predicted_order_sents:
                if sent_idx not in selected_sentences:
                    selected_sentences += [sent_idx]

            selected_positions += selected_sentences

            matched_percentage = int(
                100 * len([target_sent for target_sent in target_order_sents if target_sent in predicted_order_sents]) / summary_sents)
            if matched_percentage in successful_prediction:
                successful_prediction[matched_percentage] += 1
            else:
                successful_prediction[matched_percentage] = 1

            original_index = batch_doc_indices[doc_idx]
            article, summary = docs[original_index]

            predicted_summary = index_select(article, selected_sentences)
            target_summary = index_select(article, target_order_sents)
            lead3_summary = index_select(article, range(min(doc_len, 3)))
            lead2_summary = index_select(article, range(min(doc_len, 2)))
            random_summary = index_select(
                article, [math.floor(random.random()*doc_len) for i in range(3)])

            rouge_scores.append(_get_rouge_2(summary, predicted_summary))
            target_rouge_scores.append(_get_rouge_2(summary, target_summary))
            random_rouge_scores.append(_get_rouge_2(summary, random_summary))
            lead2_rouge_scores.append(_get_rouge_2(summary, lead2_summary))
            lead3_rouge_scores.append(_get_rouge_2(summary, lead3_summary))
